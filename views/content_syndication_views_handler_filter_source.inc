<?php

class content_syndication_views_handler_filter_source extends views_handler_filter_in_operator {

  static function sources() {
    $sources = &drupal_static(__METHOD__);
    if (!$sources) {
      $sources = db_query('SELECT name FROM {content_syndication_sources} ORDER BY name')->fetchCol();
      $sources = array_combine($sources, $sources);
    }
    return $sources;
  }

  function get_value_options() {
    $this->value_options = self::sources();
  }

}
