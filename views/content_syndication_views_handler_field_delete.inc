<?php

class content_syndication_views_handler_field_delete extends views_handler_field {

  function render($values) {
    $value = $this->get_value($values);
    $options['query']['destination'] = current_path();
    return l(t('Delete'), "admin/config/services/syndication/content/$value/delete", $options);
  }

}
