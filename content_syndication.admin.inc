<?php

/**
 * @file
 * Content Syndication administrative page callbacks.
 */

/**
 * Form constructor for managing Content Syndication sources.
 */
function content_syndication_source_form($form, &$form_state, $source = NULL) {

  if (!$source) {
    $source = new stdClass();
  }

  if (!isset($form_state['source'])) {
    $form_state['source'] = $source;
  }
  else {
    $source = $form_state['source'];
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A machine name for the source.'),
    '#required' => TRUE,
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The URL of the source feed.'),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('content_syndication_source_form_save_submit'),
  );

  // If source was provided, set defaults and tuck the source into our form so
  //  we can get back at it later.
  if (!empty($source->name)) {
    $form['name']['#default_value'] = $source->name;
    $form['url']['#default_value'] = $source->url;
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('content_syndication_source_form_delete_submit'),
    );
  }

  return $form;
}

/**
 * Submit callback to handle when a user clicks the delete button.
 */
function content_syndication_source_form_delete_submit(&$form, &$form_state) {
  drupal_goto("admin/config/services/syndication/sources/{$form_state['source']->sid}/delete");
}

/**
 * Submit callback to handle when a user clicks the save button.
 */
function content_syndication_source_form_save_submit(&$form, &$form_state) {
  $source = $form_state['source'];
  $source->name = $form_state['values']['name'];
  $source->url = $form_state['values']['url'];
  content_syndication_source_save($source);
  drupal_set_message(t('Syndication source :source has been saved.', array(':source' => $source->name)));
  $form_state['redirect'] = 'admin/config/services/syndication/sources';
}

/**
 * Form constructor for the delete confirmation on syndication sources.
 */
function content_syndication_source_delete_form($form, &$form_state, $source) {
  $form_state['source'] = $source;
  $question = t('Are you sure you want to delete syndication source %source?', array('%source' => $source->name));
  $path = 'admin/config/services/syndication/sources';
  return confirm_form($form, $question, $path);
}

/**
 * Submit callback for content_syndication_source_delete_form.
 */
function content_syndication_source_delete_form_submit(&$form, &$form_state) {
  content_syndication_source_delete($form_state['source']);
  drupal_set_message(t('Content syndication source :source has been deleted.', array(':source' => $form_state['source']->name)));
  $form_state['redirect'] = 'admin/config/services/syndication/sources';
}

/**
 * Administrative page listing all of the available syndication sources.
 */
function content_syndication_sources_admin_page() {

  // Build the list of sources.
  $rows = array();
  foreach (content_syndication_sources() as $name => $info) {
    $source = content_syndication_source_load($name);
    switch ($info['storage']) {
      case CONTENT_SYNDICATION_STORAGE_STATE_OVERRIDDEN:
        $delete = l('Revert', "admin/config/services/syndication/sources/$name/delete");
        $state = t('Overridden');
        break;
      case CONTENT_SYNDICATION_STORAGE_STATE_DATABASE:
        $delete = l('Delete', "admin/config/services/syndication/sources/$name/delete");
        $state = t('Database');
        break;
      default:
        $state = t('Default');
        $delete = '';
        break;
    }
    $rows[] = array(
      $source->name,
      $source->url,
      $state,
      l('Edit', "admin/config/services/syndication/sources/$name"),
      $delete,
      l('Refresh', "admin/config/services/syndication/sources/$name/refresh"),
      l('Export', "admin/config/services/syndication/sources/$name/export"),
    );
  }

  // Render into our table.
  $output['table'] = array(
    '#header' => array(t('Name'), t('URL'), t('State'), '', '', '', ''),
    '#rows' => $rows,
    '#theme' => 'table',
  );

  return $output;
}

/**
 * Provide the exportable verion of the source.
 */
function content_syndication_sources_export($form, &$form_state, $source) {
  ctools_include('export');
  $form['export'] = array(
    '#type' => 'textarea',
    '#title' => t('Export'),
    '#default_value' => ctools_export_object('content_syndication_sources', $source, '', 'source'),
  );
  return $form;
}

/**
 * Form constructor for the syndicated content delete confirmation form.
 */
function content_syndication_content_delete_form($form, &$form_state, $content) {
  $form['#content'] = $content;
  $args = array(
    ':title' => $content->title,
    ':source' => $content->source,
  );
  $question = t('Are you sure you want to delete :title from :source?', $args);
  $path = 'admin/config/services/syndication/content';
  return confirm_form($form, $question, $path);
}

/**
 * Form constructor for the syndicated content delete confirmation form.
 */
function content_syndication_content_delete_form_submit(&$form, &$form_state) {
  content_syndication_content_delete($form['#content']);
  drupal_set_message(t('Syndicated content :title deleted.', array(':title' => $form['#content']->title)));
}

/**
 * Form constructor to kick off syndication source batch refresh.
 */
function content_syndication_source_refresh_form($form, &$form_state, $source) {
  $form['#source'] = $source;

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#description' => t('Use this to reload the feed from the source :source.', array(':source' => $source->name)),
  );

  $form['fieldset']['delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete existing syndicated content from this source'),
  );

  $form['fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh'),
  );

  return $form;
}

/**
 * Submit callback for content_syndication_source_refresh_form.
 *
 * Kicks off a batch process to pull in all data from the feed.
 */
function content_syndication_source_refresh_form_submit(&$form, &$form_state) {

  // Delete existing syndicated content.
  // @todo: make this a batch too, could be a lot of stuff.
  if ($form_state['values']['delete']) {
    $query = new ContentSyndicationEfq();
    $query->propertyCondition('source', $form['#source']->name);
    foreach ($query->queryEntities() as $content) {
      content_syndication_content_delete($content);
    }
  }

  $batch = array(
    'title' => t('Refreshing Syndicated Content'),
    'operations' => array(
      array('content_syndication_refresh_batch', array($form['#source'])),
    ),
    'progress_message' => '@elapsed Elapsed, @estimate Remaining',
    'file' => drupal_get_path('module', 'content_syndication') . '/content_syndication.admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch operation to import syndicated content.
 */
function content_syndication_refresh_batch($source, &$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox']['params']['since'] = 1;
    $context['sandbox']['params']['page'] = 1;
  } else {
    $context['sandbox']['params']['page']++;
  }

  module_load_include('inc', 'content_syndication', 'content_syndication.syndication');
  if ($data = content_syndication_fetch_source($source, $context['sandbox']['params'])) {
    content_syndication_parse_source($source, $data->data);
    $context['finished'] = $context['sandbox']['params']['page'] / $data->pages;
  }

}
