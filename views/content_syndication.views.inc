<?php

/**
 * @file
 * Content Syndication Views hooks.
 */

/**
 * Implements hook_views_data().
 *
 * Describe syndicated content data to Views.
 */
function content_syndication_views_data() {
  $data = array();

  $data['content_syndication']['table']['group'] = t('Syndication');

  $data['content_syndication']['table']['base'] = array(
    'field' => 'cid',
    'title' => t('Syndicated content'),
    'help' => t('Syndicated content from other sites.'),
  );

  $data['content_syndication']['table']['entity type'] = 'syndicated_content';

  $data['content_syndication']['cid'] = array(
    'title' => t('CID'),
    'help' => t('Syndicated content ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['content_syndication']['nid'] = array(
    'title' => t('NID'),
    'help' => t('NID of the syndicated content.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['content_syndication']['source'] = array(
    'title' => t('Source'),
    'help' => t('Source from which the content syndicated.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'content_syndication_views_handler_filter_source',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['content_syndication']['title'] = array(
    'title' => t('Title'),
    'help' => t('Title of the syndicated content.'),
    'field' => array(
      'handler' => 'content_syndication_views_handler_field_title',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['content_syndication']['status'] = array(
    'title' => t('Published'),
    'help' => t('Published status of the syndicated content.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'published-notpublished' => array(t('Published'), t('Not published')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['content_syndication']['type'] = array(
    'title' => t('Type'),
    'help' => t('Content type of the syndicated content.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['content_syndication']['language'] = array(
    'title' => t('Language'),
    'help' => t('Langauge of the syndicated content.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'content_syndication_views_handler_filter_language',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['content_syndication']['created'] = array(
    'title' => t('Created'),
    'help' => t('The date the syndicated content was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['content_syndication']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the syndicated content was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['content_syndication']['url'] = array(
    'title' => t('URL'),
    'help' => t('URL of the syndicated content.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['content_syndication']['delete'] = array(
    'title' => t('Delete'),
    'help' => t('A link to delete the syndicated content.'),
    'field' => array(
      'real field' => 'cid',
      'handler' => 'content_syndication_views_handler_field_delete',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}
