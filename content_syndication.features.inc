<?php

/**
 * @file
 * Content Syndication Features hooks.
 */

/**
 * Implements hook_features_export().
 */
function content_syndication_source_features_export($data, &$export, $module_name) {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['content_syndication'] = 'content_syndication';
  foreach ($data as $component) {
    $export['features']['content_syndication_source'][$component] = $component;
  }
  return array();
}

/**
 * Implements hook_features_export_options().
 */
function content_syndication_source_features_export_options() {
  $sources = array_keys(content_syndication_sources());
  return array_combine($sources, $sources);
}

/**
 * Implements hook_features_export_render().
 */
function content_syndication_source_features_export_render($module_name, $data) {
  $code = array();
  $code[] = '  $sources = array();';
  $code[] = '';
  foreach ($data as $name) {
    $source = content_syndication_source_load($name);
    $code[] = ctools_export_object('content_syndication_sources', $source, '  ', 'source') . "  \$sources['{$source->name}'] = \$source;\n";
  }
  $code[] = '  return $sources;';
  $code = implode("\n", $code);
  return array('content_syndication_sources_default' => $code);
}

/**
 * Implements hook_features_revert().
 *
 * Load the defaults from code, use them to delete records from database.
 */
function content_syndication_source_features_revert($module_name) {
  content_syndication_include_all();
  foreach(module_invoke($module_name, 'content_syndication_sources_default') as $source) {
    content_syndication_source_delete($source);
  }
}
