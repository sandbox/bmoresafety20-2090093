<?php

class content_syndication_views_handler_field_title extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['link_title'] = array('default' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Link to syndicated content'),
      '#default_value' => $this->options['link_title'],
    );
  }

  function query() {
    if (TRUE) {
      $fields = array(
        'url' => array(
          'table' => 'content_syndication',
          'field' => 'url'
        ),
      );
      $this->add_additional_fields($fields);
    }
    parent::query();
  }

  function render($values) {
    $value = $this->get_value($values);

    if ($this->options['link_title']) {
      $options['attributes']['target'] = '_blank';
      return l($value, $this->get_value($values, 'url'), $options);
    }

    return $this->sanitize_value($value);
  }

}
