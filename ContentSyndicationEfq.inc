<?php

/**
 * Utility class to perform queries for syndicated content.
 */
class ContentSyndicationEfq extends EntityFieldQuery {

  var $type = 'syndicated_content';

  /**
   * Set defaults on all of the queries this class will be handling.
   */
  public function __construct() {
    $this->entityCondition('entity_type', $this->type);
  }

  /**
   * Find existing syndicated content from a source/nid combination.
   */
  public function existing($source_name, $nid) {
    $this->propertyCondition('source', $source_name);
    $this->propertyCondition('nid', $nid);
    $ids = $this->queryIds();
    return reset($ids);
  }

  /**
   * Run the query and load the entities included in the result.
   */
  public function queryEntities() {
    return entity_load($this->type, $this->queryIds());
  }

  /**
   * Run the query to fetch the entity IDs.
   */
  public function queryIds() {
    $result = $this->execute();
    if (!empty($result[$this->type])) {
      return array_keys($result[$this->type]);
    }
    return array();
  }

  /**
   * Find the last time that a source reported content as changed.
   */
  public function queryLastUpdate($source_name) {
    // Set up the query.
    $this->propertyCondition('source', $source_name);
    $this->propertyOrderBy('changed', 'DESC');
    $this->range(0, 1);

    // Get the entities from the result.
    $entities = $this->queryEntities();

    // Take the first (should be the only one, too) result.
    $entity = reset($entities);
    return $entity->changed;
  }

}
