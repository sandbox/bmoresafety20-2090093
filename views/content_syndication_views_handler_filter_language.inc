<?php

class content_syndication_views_handler_filter_language extends views_handler_filter_in_operator {

  static function langauges() {
    $langauges = &drupal_static(__METHOD__);
    if (!$langauges) {
      $langauges = db_query('SELECT DISTINCT language FROM {content_syndication} ORDER BY language')->fetchCol();
      $langauges = array_combine($langauges, $langauges);
    }
    return $langauges;
  }

  function get_value_options() {
    $this->value_options = self::langauges();
  }

}
