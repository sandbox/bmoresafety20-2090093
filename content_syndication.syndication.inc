<?php

/**
 * @file
 * Code for syndication functionality of the Content Syndication module.
 */

/**
 * Performs the HTTP request to fetch data from the source.
 */
function content_syndication_fetch_source($source, $params = array()) {

  // Deal with URL params.
  $url = $source->url;
  if (!empty($params)) {
    $url = url($url, array('query' => $params));
  }

  // Make the HTTP request.
  $data = drupal_http_request($url);

  // Bail on error.
  if (!empty($data->error)) {
    return;
  }

  // Bail if data is not able to be decoded.
  if (!$json = json_decode($data->data)) {
    return;
  }

  return $json;
}

/**
 * Save syndicated content data.
 */
function content_syndication_parse_source($source, $data) {
  // Iterate through the data received from the source, save it.
  foreach ($data as $nid => $content) {
    // Attempt to load existing syndication data.
    $query = new ContentSyndicationEfq();
    $existing = $query->existing($source->name, $nid);
    if ($existing) {
      $content->cid = $existing;
    }
    $content->source = $source->name;

    // Seed these with empty data, it'll be (re)generated on entity save.
    $content->content_syndication_attrs = array();
    $content->content_syndication_blob = array();

    module_invoke_all('content_syndication_parse', $content);
    content_syndication_content_save($content);
  }
}

/**
 * Load data that's changed since the last time the source was queried.
 */
function content_syndication_update_source($source) {

  // This process is protected by a lock, to prevent us from stepping on ourselves.
  $lock_name = __FUNCTION__ . "--{$source->name}";
  if (!lock_acquire($lock_name)) {
    return;
  }

  // Find the last time the source was queried using the timestamps provided
  // on the syndicated content.
  $query = new ContentSyndicationEfq();
  $params = array(
    'since' => $query->queryLastUpdate($source->name),
    'page' => 0,
  );

  // Attempt a query of data from the source.
  do {
    // Attempt to get some data from the source.
    $json = content_syndication_fetch_source($source, $params);
    $params['page']++;

    // If we received data, attempt to parse it.
    if (!empty($json['data'])) {
      content_syndication_parse_source($source, $json['data']);
    }
  } while ($json && $json['page'] < $json['pages']);

  lock_release($lock_name);
}
